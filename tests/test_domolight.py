import unittest
from domo.domolight import DomoLight


class TestDomoLight(unittest.TestCase):
    def test_brightness_in_init(self):
        light = DomoLight(brightness=50)
        self.assertTrue(light.is_on)
        self.assertEquals(light._brightness, 50)

    def test_bightness_setter(self):
        light = DomoLight()
        light.set_brightness(42)
        self.assertEquals(light._brightness, 42)

        
        
