from setuptools import setup, find_packages


setup(
    name='domo',
    version='0.0.1',
    url='https://example.org',
    author='Anonymous',
    author_email='anonymous@example.org',
    packages=['domo']
)
