from abc import ABC, abstractmethod

class DomoDevice(ABC):
    """ Base class of all domo devices. It's abstract.

    """

    def __init__(self):
        self.device_id = None
        self.is_on = False

    @abstractmethod
    def turn_on(self):
        """ Abstract method to turn on the device."""
        self.is_on = True

    @abstractmethod
    def turn_off(self):
        self.is_on = False

    
    def get_is_on(self):
        return self._is_on

    def set_is_on(self, value):
        self._is_on = value

        
    is_on = property(get_is_on, set_is_on)
