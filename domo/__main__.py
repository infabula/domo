from .domodevice import DomoDevice
from .domolight import DomoLight

# should not work
# device = DomoDevice()
# device.turn_on()

light = DomoLight()
light.turn_on()
print("Light on is", light.is_on)
