from .domodevice import DomoDevice


class DomoLight(DomoDevice):
    def __init__(self, brightness=0):
        super().__init__()
        self._brightness = 0
        self.set_brightness(brightness)

    def turn_on(self):
        print("Turning on the light")
        super().turn_on()

    def turn_off(self):
        print("Turning off the light")
        super().turn_off()

    def set_brightness(self, value):
        if value < 0:
            self._brightness = 0
            self.turn_off()
        elif value > 100:
            self.turn_on()
            self._brightness = 100
        else:
            self._brightness = value
            self.turn_on()
        return self
